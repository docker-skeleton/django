FROM python:3.7-alpine
RUN apk update && apk add --virtual build-deps gcc musl-dev && apk add postgresql-dev
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
CMD echo "hello world"